
(ns sagittariidae.fe.state
  (:require [re-frame.core :refer [register-sub]]
            [reagent.core  :as reagent]
            [reagent.ratom :refer-macros [reaction]]
            [schema.core   :refer [Int Keyword Num Str Bool
                                   enum maybe]]))

(def State
  "The Prismatic Schema for the Sagittariidae application state."
  (let [method {:id Str
                :name Str
                :description Str}]
    {:project {:id Str
               :name Str}
     :cached {:projects [{:id Str
                          :name Str
                          :sample-mask Str}]
              :methods [method]}
     :new-project-name Str
     :new-sample-name Str
     :error-alert Str
     :active-admin-pane Keyword ; One of :projects, :samples, :methods, :users or :roles
     :search-terms Str
     :search-results [{:id Str
                       :name Str
                       :project Str}]
     :sample {:selected {:id Str
                         :name Str
                         :project Str}
              :stages {:list  [{:id Str
                                :method Str
                                :annotation Str
                                :alt-id (maybe Str)
                                :sample Str}]
                       :token (maybe Str)}
              :active-stage {:id (maybe Str)
                             :file-spec [{:id Str
                                          :file Str
                                          :mtime Str
                                          :status (enum :unknown :processing :ready)
                                          :uri Str}]
                             :upload {:file (maybe js/Object) ; ResumableFile [1]
                                      :transmit {:progress Num
                                                 :state Keyword}
                                      :checksum {:progress Num
                                                 :state Keyword
                                                 :value Str}}}
              :new-stage {:method (maybe (conj method {:label Str
                                                       :value Str}))
                          :annotation Str}}
     :volatile {:initchan (maybe js/Object)
                :resumable js/Resumable
                :digester (maybe js/Object) ; goog.crypt.Sha256 [1]
                :checksum (maybe Str)}
     :admin-projects [{:id Str
                      :name Str
                      :sample-mask Str}]
     :admin-methods [method]
     :admin-users [{:id Int
                      :email Str
                      :display-name Str}]
     :admin-groups [{:id Int
                      :name Str
                      :description Str}]
     :admin-group-users [{:id Int
                      :group Int
                      :user Int}]
     :admin-project-users [{:id Int
                      :project Str
                      :read Bool
                      :write Bool
                      :user Int}]
     :admin-project-groups [{:id Int
                      :project Str
                      :read Bool
                      :write Bool
                      :group Int}]
     :admin-samples [{:id Str
                      :name Str
                      :project Str}]}))

;; [1] These should all be specific types of JavaScript objects.  Schema
;;     requires JS prototype functions to do type matching, and whatever these
;;     are don't fit the bill.  I need to understand the JS prototyping model
;;     better and try to figure this out.

(def null-state
  {:cached {:projects []
            :methods []}
   :project {:id ""
             :name ""}
   :new-project-name ""
   :new-sample-name ""
   :search-terms ""
   :error-alert ""
   :active-admin-pane :projects
   :search-results []
   :sample {:selected {:id ""
                       :name ""
                       :project ""}
            :stages {:list []
                     :token nil}
            :active-stage {:id nil
                           :file-spec []
                           :upload {:file nil
                                    :transmit {:progress 0.0
                                               :state :default}
                                    :checksum {:progress 0.0
                                               :state :default
                                               :value ""}}}
            :new-stage {:method nil
                        :annotation ""}}
   :volatile {:initchan nil
              :resumable nil
              :digester nil
              :checksum nil}
   :admin-projects []
   :admin-methods []
   :admin-users []
   :admin-groups []
   :admin-group-users []
   :admin-project-users []
   :admin-project-groups []
   :admin-samples []})

(def state
  (reagent/atom null-state))

(defn clear
  "Reset the part of the state tree denoted by `path` to its default state."
  [state path]
  (assoc-in state path (get-in null-state path)))

(defn copy-state
  "Copy elements of the application state from `src` to `dst`; `paths` is a
  collection of sequences, where each is a path that might be used with
  `assoc-in` or `get-in`.  This function makes no attempt to optimise the
  process by looking for longer paths that may be contained within shorter
  ones."
  [dst src paths]
  (loop [paths paths
         dst   dst]
    (if (seq paths)
      (let [path (first paths)]
        (recur (rest paths)
               (assoc-in dst path (get-in src path))))
      dst)))

;; ------------------------------------------------------- admin stuff ----- ;;

(register-sub
 :query/admin-projects
 (fn [state [query-id]]
   (assert (= query-id :query/admin-projects))
   (reaction (get @state :admin-projects))))

(register-sub
 :query/admin-samples
 (fn [state [query-id]]
   (assert (= query-id :query/admin-samples))
   (reaction (get @state :admin-samples))))

(register-sub
 :query/admin-methods
 (fn [state [query-id]]
   (assert (= query-id :query/admin-methods))
   (reaction (get @state :admin-methods))))
(register-sub
 :query/admin-users
 (fn [state [query-id]]
   (assert (= query-id :query/admin-users))
   (reaction (get @state :admin-users))))
(register-sub
 :query/admin-groups
 (fn [state [query-id]]
   (assert (= query-id :query/admin-groups))
   (reaction (get @state :admin-groups))))
(register-sub
 :query/admin-group-users
 (fn [state [query-id]]
   (assert (= query-id :query/admin-group-users))
   (reaction (get @state :admin-group-users))))
(register-sub
 :query/admin-project-users
 (fn [state [query-id]]
   (assert (= query-id :query/admin-project-users))
   (reaction (get @state :admin-project-users))))
(register-sub
 :query/admin-project-groups
 (fn [state [query-id]]
   (assert (= query-id :query/admin-project-groups))
   (reaction (get @state :admin-project-groups))))

;; ------------------------------------------------------- subscriptions --- ;;

(register-sub
 :query/ui-enabled?
 (fn [state [query-id]]
   (assert (= query-id :query/ui-enabled?))
   (letfn [(neither-nil-nor-empty? [x]
             (and (not (nil? x)) (not (empty? x))))]
     (reaction (neither-nil-nor-empty? (:name (:project @state)))))))

(register-sub
 :query/projects
 (fn [state [query-id]]
   (assert (= query-id :query/projects))
   (reaction (get-in @state [:cached :projects]))))

(register-sub
 :query/methods
 (fn [state [query-id]]
   (assert (= query-id :query/methods))
   (reaction (get-in @state [:cached :methods]))))

(register-sub
 :query/active-project
 (fn [state [query-id]]
   (assert (= query-id :query/active-project))
   (reaction (:project @state))))

(register-sub
 :query/error-alert
 (fn [state [query-id]]
   (assert (= query-id :query/error-alert))
   (reaction (:error-alert @state))))

(register-sub
 :query/active-admin-pane
 (fn [state [query-id]]
   (assert (= query-id :query/active-admin-pane))
   (reaction (:active-admin-pane @state))))

(register-sub
 :query/sample-search-terms
 (fn [state [query-id]]
   (assert (= query-id :query/sample-search-terms))
   (reaction (get-in @state [:search-terms]))))

(register-sub
 :query/sample-search-results
 (fn [state [query-id]]
   (assert (= query-id :query/sample-search-results))
   (reaction (get @state :search-results))))

(register-sub
 :query/selected-sample
 (fn [state [query-id]]
   (assert (= query-id :query/selected-sample))
   (reaction (get-in @state [:sample :selected]))))

(register-sub
 :query/sample-stages
 (fn [state [query-id]]
   (assert (= query-id :query/sample-stages))
   (reaction {:stages     (get-in @state [:sample :stages :list])
              :next-stage (get-in @state [:sample :stages :token])
              :active     (get-in @state [:sample :active-stage :id])})))

(register-sub
 :query/sample-stage-detail
 (fn [state [query-id]]
  (assert (= query-id :query/sample-stage-detail))
  (reaction (get-in @state [:sample :active-stage]))))

(register-sub
 :query/sample-stage-input
 (fn [state [query-id]]
   (assert (= query-id :query/sample-stage-input))
   (reaction (-> @state
                 (get-in [:sample :new-stage])
                 (assoc :id (get-in @state [:sample :stages :token]))))))
