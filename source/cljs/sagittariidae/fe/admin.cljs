(ns sagittariidae.fe.admin
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljsjs.react-bootstrap]
            [cljsjs.react-select]

            [clojure.string  :as           str]
            [cljs.core.async :refer        [chan <!]]
            [re-frame.core   :refer        [dispatch dispatch-sync subscribe]]
            [reagent.core    :refer        [adapt-react-class render]]
            [reagent.core    :as           reagent]
            [reagent.ratom   :refer-macros [reaction]]

            [sagittariidae.fe.event]
            [sagittariidae.fe.env           :as    env]
            [sagittariidae.fe.state         :refer [null-state]]
            [sagittariidae.fe.reagent-utils :as    utils]
            [sagittariidae.fe.util          :refer [pairs->map]]))

;;; -------------------------------------------------- adapted components --- ;;

(defn react-bootstrap->reagent
  [c]
  (adapt-react-class (aget js/ReactBootstrap (str c))))

(def button          (react-bootstrap->reagent 'Button))
(def column          (react-bootstrap->reagent 'Col))
(def dropdown-button (react-bootstrap->reagent 'DropdownButton))
(def form-control    (react-bootstrap->reagent 'FormControl))
(def glyph-icon      (react-bootstrap->reagent 'Glyphicon))
(def grid            (react-bootstrap->reagent 'Grid))
(def menu-item       (react-bootstrap->reagent 'MenuItem))
(def nav-dropdown    (react-bootstrap->reagent 'NavDropdown))
(def progress-bar    (react-bootstrap->reagent 'ProgressBar))
(def row             (react-bootstrap->reagent 'Row))
(def alert           (react-bootstrap->reagent 'Alert))

(def select          (adapt-react-class js/Select))

;; ----------------------------------------------- composable components --- ;;

(defn- component:table
  "A generalised component for building rendering tables.  The table content is
  described by `spec`, a map of the form:
  ```
  {:column-name {:label \"RenderName\" :data-fn #(transform %)} ...}
  ```
  Note that while this is arguably an ugly hack, it is convenient for us to
  assume that our table spec is defined as a literal map (rather than being
  programmatically constructed) and that it contains a small number of
  columns (< 9).  Clojure(Script) optimises such maps to PersistentArrayMaps
  which preserve their insertion order."
  [spec rows]
  [:table.table.table-condensed.table-striped.table-hover
   {:style {:background-color "#fafafa"}}
   ;; Note: Attempting to deref a Reagent atom inside a lazy seq can cause
   ;; problems, because the execution context could move from the component in
   ;; which lazy-deq is created, to the point at which it is expanded.  (In the
   ;; example below, the lazy-seq return by the `for` loop wouldn't be expanded
   ;; until the `:tr` is expanded, at which point the atom no longer knows that
   ;; the intent was to have it deref'd in this component.
   ;;
   ;; See the following issue for a more detailed discussion of this issue:
   ;; https://github.com/reagent-project/reagent/issues/18
   (utils/key
    (list
     [:thead
      [:tr (doall
            (for [colkey (keys spec)]
              (let [label (get-in spec [colkey :label])]
                ^{:key label}
                [:th label])))]]
     [:tbody
      (doall
       (for [row rows]
         (do
           (when-not (or (get row :id) (get row (:id-key spec)))
             (throw (ex-info "No `:id` and no ID key found for row data.  Row data must include an `:id` key, or the table spec must include an `:id-key` (that identifies the row column to use as the ID field); this is required to provide the required ReactJS `key` for dynamically generated children, and must provide an *identity* for the row, not just an index." row)))
           ^{:key (:id row)}
           [:tr (doall
                 (for [colkey (keys spec)]
                   (let [data-fn (or (get-in spec [colkey :data-fn]) (fn [x _] x))]
                     ^{:key colkey}
                     [:td (data-fn (get row colkey) row)])))])))]))])

(defn component:text-input-action
  [{:keys [placeholder value on-change on-click enabled?]
    :or   {enabled true}}]
  [:div.input-group
   [:input.form-control
    {:type        "text"
     :placeholder placeholder
     :value       value
     :on-change   #(on-change (-> % .-target .-value))
     :disabled    (not enabled?)}]
   [:span.input-group-btn
    [:button.btn.btn-default
     {:type       "button"
      :on-click   #(on-click)
      :disabled   (not enabled?)}
     [:span.glyphicon.glyphicon-download]]]])

(defn component:select
  [{:keys [options value enabled?]}]
  [select {:options   options
           :value     value
           :on-change #(dispatch [:event/stage-method-selected %])
           :disabled  (not enabled?)}])

;; ------------------------------------------------ top level components --- ;;

(defn component:alert-bar
  []
  (let [alerts (subscribe [:query/error-alert])]
    (fn []
      [:div (if (= @alerts "") [:div]
        ; Why do I have to do this manually instead of using the alert component from react? ¯\_(ツ)_/¯
       [:div.alert.alert-danger.alert-dismissible.show
        @alerts
         [:button {:type "button" :class "close" :on-click #(dispatch [:event/error-alert ""]) :aria-label "Close"}
    [:span {:aria-hidden "true"} "×"]]])])))

(defn- addListItem [pane name]
  (let [active-pane (subscribe [:query/active-admin-pane])
        onclick #(dispatch [:event/change-admin-pane pane])]
    (if (= @active-pane pane)
      [:li.nav-item.active [:a.nav-link {:href "#" :on-click onclick} name]]
      [:li.nav-item        [:a.nav-link {:href "#" :on-click onclick} name]]
    )))

(defn- addDiv [pane id]
  (let [active-pane (subscribe [:query/active-admin-pane])]
    (if (= @active-pane pane) [:div {:id id}] [:div {:style {:display "none"} :id id}])
  )
)

(defn component:admin-panel
  []
    (fn []
      [:ul.nav.nav-tabs
        (addListItem :projects "Projects")
        (addListItem :samples "Samples")
        (addListItem :methods "Methods")
        (addListItem :users "Users")
        (addListItem :groups "Groups")
        (addListItem :group-user "Groups<->Users")
        (addListItem :project-user "project<->Users")
        (addListItem :project-group "project<->Groups")
      ]))

(defn component:containers
  []
  [:div
    (addDiv :projects "project-table")
    (addDiv :samples "sample-table")
    (addDiv :methods "method-table")
    ; (addDiv :users "Users")
    (addDiv :users "user-table")
    (addDiv :project-user "project-user-table")
    (addDiv :project-group "project-group-table")
    (addDiv :group-user "group-user-table")
    (addDiv :groups "group-table")
  ]
)

(defn component:pickable
  [allOfThem onPick initial prefix name-sym]
  (let [selected (reagent/atom {:got :none})]
    (fn []
      (if (= (get @selected :got) :none) (swap! selected assoc :got initial) ())
      [:div
          [nav-dropdown
          {:id       "nav-project-dropdown"
            :title    
                     (if (or (nil? (get @selected :got)) (empty? (get @selected :got)))
                        "none"
                        (get-in @selected [:got name-sym]))}
          (for [option allOfThem]
              ^{:key (get option :id)} [menu-item {:on-click (fn [] (swap! selected assoc :got option) (onPick option))} (get option name-sym)])]]
  )))

(defn component:editable-table
  [list-of-them editable-elements full-name can-delete]
  (let [editing-fake (reagent/atom {:at :none :content {}})
        num-elements (count list-of-them)
        num-columns (count editable-elements)
        endpoint ["admin" (str full-name "s")]
        path [(keyword (str "admin-" (str full-name "s")))]]
  (fn [] 
    (let [editing @editing-fake] [:table.table-sm.table.table-striped.table-responsive.editable [:thead
    [:tr
     ; Draw the headers
    (for [[element-name] editable-elements]
      ^{:key element-name} [:th [:h3 (name element-name)]]
        )
      [:th]]]
    [:tbody (for [[i one-of-them] (zipmap (range) list-of-them)] 
      ^{:key i}
        [:tr
        (for [[element-name size editable-type] editable-elements]
          (if (= (get editing :at) i)
            ^{:key element-name}
            [:td
              (case editable-type
                :text [form-control {:type        "text"
                              :placeholder  (get one-of-them element-name)
                              :defaultValue (get one-of-them element-name)
                              :on-change   #(swap! editing-fake assoc-in [:content element-name] (-> % .-target .-value))
                              :disabled    false}]
                :bool [form-control {:type        "checkbox"
                              :defaultValue (get one-of-them element-name)
                              :on-change   #(swap! editing-fake assoc-in [:content element-name] (-> % .-target .-checked))
                              :disabled    false}]
                [(component:pickable (first editable-type) #(swap! editing-fake assoc-in [:content element-name] %) (get-in editing [:content element-name]) element-name (second editable-type))])]
            ^{:key element-name}
            [:td
              (case editable-type
              :text [:p (get one-of-them element-name)]
              :bool [form-control {:type        "checkbox"
                            :defaultChecked (get one-of-them element-name)
                            :disabled    true}]
              [:p (get-in one-of-them [element-name (second editable-type)])])]))
          (if (= (get editing :at) i)
            [:td {:md 3}
              [button {:on-click #(reset! editing-fake {:at :none :content {}})}
                [glyph-icon {:glyph "chevron-left"}]]
              [button {:on-click #((reset! editing-fake {:at :none :content {}})
                                   (dispatch [:event/update-admin-thing i endpoint (get editing :content) path full-name]))}
                [glyph-icon {:glyph "floppy-disk"}]]
              (if can-delete 
              [:button.btn.btn-danger {:type "button"
                                       :on-click #((reset! editing-fake {:at :none :content {}})
                                   (dispatch [:event/delete-admin-thing i endpoint (get editing :content) path full-name]))}
                [glyph-icon {:glyph "remove"}]]
              [:div])]
            [:td {:md 1}
              [button {:on-click #(reset! editing-fake {:at i :content one-of-them})}
                [glyph-icon {:glyph "pencil"}]]]
          )])
      (if (and (empty? list-of-them) (not= (get editing :at) :new))
        [:tr [:td.empty-placeholder
        {:col-span num-columns}
        [:h4 "There are currently no items"]]])
      (if (= (get editing :at) :new)
        [:tr
         (for [[element-name size editable-type] editable-elements]
          ^{:key element-name}
          [:td {:md size}
              (case editable-type
              :text [form-control {:type        "text"
                              :placeholder (str "New " (name element-name))
                              :on-change   #(swap! editing-fake assoc-in [:content element-name] (-> % .-target .-value))
                              :disabled    false}]
              :bool [form-control {:type        "checkbox"
                            :defaultChecked     false
                            :on-change   #(swap! editing-fake assoc-in [:content element-name] (-> % .-target .-checked))
                            :disabled    false}]
              [(component:pickable (first editable-type) #(swap! editing-fake assoc-in [:content element-name] %) (get-in editing [:content element-name]) element-name (second editable-type))])])
          [:td {:md 3}
            [button {:on-click #(dispatch [:event/new-admin-thing endpoint (get editing :content) path full-name])}
              [glyph-icon {:glyph "floppy-disk"}]]
            [button {:on-click #(reset! editing-fake {:at :none :content {}})}
              [glyph-icon {:glyph "chevron-left"}]]]]
        [:tr
          [:td {:md 1 :col-span num-columns}
            [button {:on-click #(reset! editing-fake {:at :new 
                                                      :content (reduce (fn [dict item] (assoc dict (first item) (if (= (get item 2) "text") "" (get-in item [2 0])))) {} editable-elements)})}
            [glyph-icon {:glyph "plus"}]]]]
         )]]))))

(defn component:project-table
  []
  (let [projects       (subscribe [:query/admin-projects])
        active-project (subscribe [:query/active-project])
        project-name   (reaction (:name @active-project))]
    ; (dispatch-sync [:event/admin-get-projects])
    (fn []
      [:div
       [(component:editable-table @projects [[:name 2 :text] [:sample-mask 2 :text]] "project" false)]])))

(defn component:method-table
  []
  (let [projects       (subscribe [:query/admin-methods])]
    (fn []
      [:div
       [(component:editable-table @projects [[:name 2 :text] [:description 2 :text]] "method" false)]])))

(defn component:user-table
  []
  (let [users       (subscribe [:query/admin-users])]
    (fn []
      [:div
       [(component:editable-table @users [[:email 2 :text] [:display-name 2 :text]] "user" true)]])))
(defn component:group-table
  []
  (let [groups       (subscribe [:query/admin-groups])]
    (fn []
      [:div
       [(component:editable-table @groups [[:name 2 :text] [:description 2 :text]] "group" true)]])))

(defn component:sample-table
  []
  (let [projects       (subscribe [:query/admin-projects])
        samples (subscribe [:query/admin-samples])]
    (fn []
      (let [samples-real (mapv #(assoc % :project 
                                       (first (filter (fn [project] (= (get % :project) (get project :id))) @projects)))@samples)]
      [(component:editable-table samples-real [[:name 2 :text] [:project 2 [@projects :name]]] "sample" false)]))))

(defn component:group-user-table
  []
  (let [users       (subscribe [:query/admin-users])
        groups (subscribe [:query/admin-groups])
        group-users (subscribe [:query/admin-group-users])]
    (fn []
      (let [group-users-real (mapv #(-> % 
                                        (assoc :user
                                               (first (filter (fn [project] 
                                                  (= (get % :user) 
                                                     (get project :id))) @users))
                                               )
                                        (assoc :group 
                                               (first (filter (fn [project] 
                                                  (= (get % :group) 
                                                     (get project :id))) @groups))
                                        )) @group-users)]
      [(component:editable-table group-users-real [[:user 2 [@users :email]] [:group 2 [@groups :name]]] "group-user" true)]))))

(defn component:project-user-table
  []
  (let [users       (subscribe [:query/admin-users])
        projects (subscribe [:query/admin-projects])
        projects-users (subscribe [:query/admin-project-users])]
    (fn []
      (let [projects-users-real (mapv #(-> % 
                                        (assoc :user
                                               (first (filter (fn [project] 
                                                  (= (get % :user) 
                                                     (get project :id))) @users))
                                               )
                                        (assoc :project 
                                               (first (filter (fn [project] 
                                                  (= (get % :project) 
                                                     (get project :id))) @projects))
                                        )) @projects-users)]
      [(component:editable-table projects-users-real [[:user 2 [@users :email]] [:project 2 [@projects :name]] [:read 1 :bool] [:write 1 :bool]] "project-user" true)]))))

(defn component:project-group-table
  []
  (let [groups       (subscribe [:query/admin-groups])
        projects (subscribe [:query/admin-projects])
        projects-users (subscribe [:query/admin-project-groups])]
    (fn []
      (let [projects-users-real (mapv #(-> % 
                                        (assoc :group
                                               (first (filter (fn [project] 
                                                  (= (get % :group) 
                                                     (get project :id))) @groups))
                                               )
                                        (assoc :project 
                                               (first (filter (fn [project] 
                                                  (= (get % :project) 
                                                     (get project :id))) @projects))
                                        )) @projects-users)]
      [(component:editable-table projects-users-real [[:group 2 [@groups :name]] [:project 2 [@projects :name]] [:read 1 :bool] [:write 1 :bool]] "project-group" true)]))))

;; --------------------------------------------------------- entry point --- ;;

(defn- by-id
  [el]
  (.getElementById js/document el))

(defn- add-component
  [c el]
  (render c (by-id el)))

(defn- make-resumable-js
  []
  (js/Resumable. #js {:target (str/join [(env/get-var :upload :host "") "/upload-part"])

                      ;; FIXME: Resumable v1.0.2 doesn't support these
                      ;; options but they are available in mainline.  It
                      ;; would be nice to put them in place when they do
                      ;; become available, because it would clean up the API
                      ;; and make it less Resumable-specific.
                      ;;
                      ;; :currentChunkSizeParameterName "part-size"
                      ;; :chunkNumberParameterName      "part-number"
                      ;; :totalChunksParameterName      "total-parts"
                      ;; :totalSizeParameterName        "file-size"
                      ;; :identifierParameterName       "upload-id"
                      ;; :fileNameParameterName         "file-name"
                      ;; :typeParameterName             "file-type"

                      :testChunks         false
                      :maxFiles           1
                      :maxChunkRetries    9
                      :chunkRetryInterval 900

                      ;; Large uploads with many simultaneous connections
                      ;; tend to result in 'network connection was lost'
                      ;; errors.  Various articles point the finger at
                      ;; Safari/MacOS (I've seen the same error in Chrome on
                      ;; MacOS) not handling Keep-Alive connections
                      ;; correctly.
                      ;;
                      ;; What I think is happening here though is that the
                      ;; server follow a strict request-response protocol.
                      ;; Thus the upload has to finish within the Keep-Alive
                      ;; timeout, since the server isn't streaming Keep-Alive
                      ;; data back to us while the upload is in progress.
                      :simultaneousUploads 3
                      :chunkSize           (* 512 1024)}))

(defn- initialize-components
  [state]
  ;; Initialize the application state so that components have sensible defaults
  ;; for their first render.  Synchronous "dispatch" ensures that the
  ;; initialization is complete before any of the components are created.
  (add-component [component:alert-bar]
                 "alert-bar")
  (add-component [component:containers]
                 "containers")
  (add-component [component:admin-panel]
                 "admin-panel")
  (add-component [component:sample-table]
                 "sample-table")
  (add-component [component:project-table]
                 "project-table")
  (add-component [component:user-table]
                 "user-table")
  (add-component [component:group-table]
                 "group-table")
  (add-component [component:group-user-table]
                 "group-user-table")
  (add-component [component:project-user-table]
                 "project-user-table")
  (add-component [component:project-group-table]
                 "project-group-table")
  (add-component [component:method-table]
                 "method-table"))

(defn- parse-url
  ;; https://gist.github.com/jlong/2428561
  [url]
  (let [p (.createElement js/document "a")]
    (set! (.-href p) url)
    p))

(defn- unpack-query-params
  [param-str]
  (let [m (->> (str/split param-str #"\?")
               (map #(str/split % "="))
               (keep not-empty)
               (pairs->map))]
    (.debug js/console "Unpacked query parameters:" (clj->js m))
    (not-empty m)))

(defn main
  []
  (let [initchan  (chan)
        resumable (make-resumable-js)]
    (dispatch-sync [:event/initializing
                    {:volatile (merge (get null-state :volatile)
                                      {:initchan  initchan
                                       :resumable resumable})}])
    (initialize-components @re-frame.db/app-db)
    ;;
    (when-let [init-path
               (:p (unpack-query-params
                    (.-search
                     (parse-url
                      (-> js/window .-location .-href)))))]
      (.debug js/console "Starting base initialization using path:" init-path)
      (go
        ;; FIXME: time out if nothing becomes available
        (<! initchan) ;; projects
        (<! initchan) ;; methods
        (.debug js/console "Base initialization complete.")
        (dispatch-sync [:event/initialize-resource-path init-path])))))
