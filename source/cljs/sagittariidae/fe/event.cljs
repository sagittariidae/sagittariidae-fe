
(ns sagittariidae.fe.event
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [clojure.string
             :as s]
            [cljs.core.async
             :refer [>!]]
            [ajax.core
             :refer [GET POST PUT DELETE]]
            [goog.crypt
             :as gcrypt]
            [re-frame.core
             :refer [dispatch register-handler]]
            [schema.core
             :refer [validate]]
            [sagittariidae.fe.checksum
             :refer [array-buffer->utf8-array utf8-array->hex-string]]
            [sagittariidae.fe.env
             :as env]
            [sagittariidae.fe.file
             :refer [process-file-chunks]]
            [sagittariidae.fe.state
             :refer [State clear copy-state null-state]]
            [sagittariidae.fe.util
             :refer [->id pairs->map]])
  (:import [goog.crypt Sha256]))

;; -------------------------------------------------------- server comms --- ;;

(def ^{:dynamic true} ajax-endpoint (env/get-var :service :host))

(defn endpoint
  [rpath]
  (s/join "/" (concat [ajax-endpoint] rpath)))

(defn urify [x]
  (-> x (s/trim) (js/encodeURIComponent)))

(def ajax-default-params
  {:response-format :json
   :keywords?       :true
   :error-handler   (fn [e]
                      (.error js/console "AJAX error: " (clj->js e)))})

(def ajax-default-mutating-params
  (conj ajax-default-params
        {:format          :json
         :response-format :json
         :keywords?       :true}))

(defn ajax-get
  [resource params]
  (let [res-uri (endpoint resource)]
    (.info js/console "GET %s" res-uri)
    (apply GET
           (flatten (concat [res-uri]
                            (seq (-> ajax-default-params (conj params))))))))

(defn- ajax-mutate
  [action resource data params]
  (let [res-uri (endpoint resource)]
    (.info js/console (str action " " res-uri ";" data))
    (apply action
           (flatten (concat [res-uri]
                            (seq (-> ajax-default-mutating-params
                                     (conj params)
                                     (assoc :params data))))))))

(defn ajax-put
  [resource data params]
  (ajax-mutate PUT resource data params))

(defn ajax-post
  [resource data params]
  (ajax-mutate POST resource data params))

(defn ajax-delete
  [resource data params]
  (ajax-mutate DELETE resource data params))

;; ---------------------------------------------------------- middleware --- ;;

(defn validate-state
  [handler]
  (fn [state event-vectr]
    (.warn js/console "Got state" (clj->js state) (clj->js event-vectr))
    (validate State (handler state event-vectr))))

;; ---------------------------------------------------------------- init --- ;;

(register-handler
 :event/initializing
 [validate-state]
 (fn [state [_ initial-state]]
   (ajax-get ["projects"]
             {:handler #(dispatch [:event/projects-retrieved %])})
   (ajax-get ["methods"]
             {:handler #(dispatch [:event/methods-retrieved %])})
   (ajax-get ["admin" "projects"]
             {:handler #(dispatch [:event/admin-projects-retrieved %])})
   (ajax-get ["admin" "samples"]
             {:handler #(dispatch [:event/admin-samples-retrieved %])})
   (ajax-get ["admin" "methods"]
             {:handler #(dispatch [:event/admin-methods-retrieved %])})
   (ajax-get ["admin" "users"]
             {:handler #(dispatch [:event/admin-users-retrieved %])})
   (ajax-get ["admin" "groups"]
             {:handler #(dispatch [:event/admin-groups-retrieved %])})
   (ajax-get ["admin" "group-users"]
             {:handler #(dispatch [:event/admin-group-users-retrieved %])})
   (ajax-get ["admin" "project-users"]
             {:handler #(dispatch [:event/admin-project-users-retrieved %])})
   (ajax-get ["admin" "project-groups"]
             {:handler #(dispatch [:event/admin-project-groups-retrieved %])})
   (-> (if (empty? state)
         null-state
         state)
       (conj initial-state))))

(register-handler
 :event/initialize-resource-path
 [validate-state]
 (fn [state [_ path]]
   (let [{project-id :projects sample-id :samples stage-id :stages} ;
         (pairs->map
          (partition 2 (keep not-empty (s/split path #"/"))))
         [project-id sample-id stage-id]
         (map ->id [project-id sample-id stage-id])
         project
         (and project-id (some #(and (= (first (s/split (:id %) #"-")) project-id) %)
                               (get-in state [:cached :projects])))]
     (when project
       (dispatch [:event/project-selected (select-keys project [:id :name])]))
     (when sample-id
       (dispatch [:event/sample-fetch-requested sample-id]))
     (comment (when stage-id
                (dispatch [:event/stage-selected stage-id]))))
   state))

(register-handler
 :event/>!-initchan
 [validate-state]
 (fn [state [_ msg]]
   (go (>! (get-in state [:volatile :initchan]) msg))
   state))

;; --- Admin things -------------------------------------------------------- ;;

;; Project admin getter ;;
(register-handler
  :event/admin-get-projects
  [validate-state]
  (fn [state [_]]
   (.warn js/console "Got into project getter" (clj->js state))
   (ajax-get ["admin" "projects"]
             {:handler #(dispatch [:event/admin-projects-retrieved %])})
  ))

(register-handler
  :event/admin-projects-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-projects projects)
  ))
(register-handler
  :event/admin-users-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-users projects)
  ))
(register-handler
  :event/admin-groups-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-groups projects)
  ))
(register-handler
  :event/admin-project-users-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-project-users (if (= projects "") [] projects))
  ))
(register-handler
  :event/admin-project-groups-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-project-groups (if (= projects "") [] projects))
  ))
(register-handler
  :event/admin-group-users-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-group-users (if (= projects "") [] projects))
  ))
;; Sample admin getter ;;
(register-handler
  :event/admin-get-samples
  [validate-state]
  (fn [state [_]]
   (.warn js/console "Got into sample getter" (clj->js state))
   (ajax-get ["admin" "samples"]
             {:handler #(dispatch [:event/admin-samples-retrieved %])})
  ))

(register-handler
  :event/admin-samples-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-samples projects)
  ))

(register-handler
  :event/admin-methods-retrieved
  [validate-state]
  (fn [state [_ projects]]
    (assoc state :admin-methods projects)
  ))
;; Method admin getter ;;
;; Method admin UI ;;

(register-handler
 :event/admin-thing-changed
 [validate-state]
 (fn [state [_ i element-name one-of-them path value]]
   (.warn js/console "Got value" (clj->js value) (clj->js state))
   (assoc-in state (conj path i element-name) value)))

(register-handler
 :event/update-admin-thing
 [validate-state]
 (fn [state [_ i endpoint new-thing path name]]
  (let [real-new-thing (into {} (map #(if (map? (second %)) [(first %) (get (second %) :id)] %) new-thing))]
  (.warn js/console "Got thing" (clj->js real-new-thing) (clj->js state))
  (ajax-put (conj endpoint (get real-new-thing :id))
            real-new-thing
            {:handler         #(dispatch [:event/admin-complete-update i path %])
              :error-handler   #(dispatch [:event/error-alert (str "Unable to update " name)])}))))

(register-handler
 :event/delete-admin-thing
 [validate-state]
 (fn [state [_ i endpoint new-thing path name]]
  (ajax-delete (conj endpoint (get new-thing :id))
            {}
            {:handler         #(dispatch [:event/admin-complete-delete i path %])
              :error-handler   #(dispatch [:event/error-alert (str "Unable to delete " name)])})))

(register-handler
 :event/new-admin-thing
 [validate-state]
 (fn [state [_ endpoint new-thing path name]]
  (let [real-new-thing (into {} (map #(if (map? (second %)) [(first %) (get (second %) :id)] %) new-thing))]
  (.warn js/console "Got thing" (clj->js real-new-thing) (clj->js state))
  (ajax-post endpoint
             real-new-thing
             { :handler         #(dispatch [:event/admin-complete-new path %])
               :error-handler   #(dispatch [:event/error-alert (str "Unable to create " name)])}))))

(register-handler
 :event/admin-complete-update
 [validate-state]
 (fn [state [_ i path response]]
    (.warn js/console "Got r" (clj->js response) (clj->js path))
   (assoc-in state (conj path i) response)))

(register-handler
 :event/admin-complete-delete
 [validate-state]
 (fn [state [_ i path response]]
   (assoc-in state path response)))

(register-handler
 :event/admin-complete-new
 [validate-state]
  (fn [state [_ path response]] 
    (.warn js/console "Got r" (clj->js response))
    (assoc-in state path (conj (get-in state path) response))))

;; --- static data --------------------------------------------------------- ;;

(register-handler
 :event/methods-retrieved
 [validate-state]
 (fn [state [_ methods]]
   (.info js/console "Received methods" (clj->js methods))
   (dispatch [:event/>!-initchan :methods])
   (assoc-in state [:cached :methods] methods)))

(register-handler
 :event/projects-retrieved
 [validate-state]
 (fn [state [_ projects]]
   (.info js/console "Received projects" (clj->js projects))
   (dispatch [:event/>!-initchan :projects])
   (assoc-in state [:cached :projects] projects)))

(register-handler
 :event/project-name-changed
 [validate-state]
 (fn [state [_ i project-name]]
   ; (.info js/console "Received projects" (clj->js projects))
   ; (dispatch [:event/>!-initchan :projects])
   (assoc-in state [:cached :projects i :name] project-name)))

(register-handler
 :event/update-project-name
 [validate-state]
 (fn [state [_ id i]]
   ; (.info js/console "Received projects" (clj->js projects))
   ; (dispatch [:event/>!-initchan :projects])
  (ajax-put ["projects" id]
            { :id id
              :name (get-in state [:cached :projects i :name])}
            {:handler         #(dispatch [:event/update-project-name-complete i %])
              :error-handler   #(dispatch [:event/error-alert "Unable to update project name."])})))

(register-handler
 :event/update-project-name-complete
 ; [validate-state]
 (fn [state [_ i response]]
   (.warn js/console "Got project" (clj->js state))
   (-> state
     (assoc-in [:cached :projects i] response)
     (assoc :search-results [])
     (assoc :new-sample-name "")
     (assoc :project {:name "" :id ""}))))

(register-handler
 :event/update-method-name-complete
 ; [validate-state]
 (fn [state [_ i response]]
   (.warn js/console "Got method" (clj->js state))
   (-> state
     (assoc-in [:cached :methods i] response))))

(register-handler
 :event/error-alert
 ; [validate-state]
 (fn [state [_ error]] (assoc state :error-alert error)))

(register-handler
 :event/change-admin-pane
 [validate-state]
 (fn [state [_ pane]] 
   (-> state 
       (assoc :active-admin-pane pane)
       (assoc :search-results [])
       (assoc :project {:id "" :name ""}))))

(register-handler
 :event/new-project-name-changed
 [validate-state]
 (fn [state [_ project-name]]
   ; (.info js/console "Received projects" (clj->js projects))
   ; (dispatch [:event/>!-initchan :projects])
   (assoc state :new-project-name project-name)))

(register-handler
 :event/update-new-project-name
 [validate-state]
 (fn [state [_]]
   ; (.info js/console "Received projects" (clj->js projects))
   ; (dispatch [:event/>!-initchan :projects])
  (let [name (get state :new-project-name)]
    (ajax-post ["projects"]
              { :name name }
              { :handler         #(dispatch [:event/update-new-project-name-complete %])
                :error-handler   #(dispatch [:event/error-alert "Unable to add new project."])}))))

(register-handler
 :event/update-new-project-name-complete
 ; [validate-state]
 (fn [state [_ response]] (assoc-in state [:cached :projects] (conj (get-in state [:cached :projects]) response))))

(register-handler
 :event/update-new-method-name-complete
 ; [validate-state]
 (fn [state [_ response]] (assoc-in state [:cached :methods] (conj (get-in state [:cached :methods]) response))))

(register-handler
 :event/project-selected
 [validate-state]
 (fn [state [_ project]]
   (-> state
       (copy-state state [[:cached]
                          [:volatile]])
       (assoc :project project))))

(register-handler
 :event/project-selected-load-samples
 [validate-state]
 (fn [state [_ id project]]
   (ajax-get [(str "projects/" id "/samples")]
             {:handler #(dispatch [:event/samples-for-admin %])})
   (assoc state :project project)))

;; ------------------------------------------------------- sample search --- ;;

(register-handler
 :event/samples-for-admin
 [validate-state]
 (fn [state [_ ret]]
   (let [samples (mapv #(select-keys % [:id :name :project]) (if (map? ret) [ret] ret))]
     (.info js/console "%d samples returned" (count samples))
     (assoc state :search-results samples))))

(register-handler
 :event/sample-name-changed
 [validate-state]
 (fn [state [_ i sample-name]]
   (assoc-in state [:search-results i :name] sample-name)))

(register-handler
 :event/update-sample-name
 [validate-state]
 (fn [state [_ pid id i]]
   ; (.info js/console "Received projects" (clj->js projects))
   ; (dispatch [:event/>!-initchan :projects])
  (ajax-put ["projects" pid "samples"]
            { :id id
              :name (get-in state [:search-results i :name])}
            { :handler         #(dispatch [:event/update-sample-name-complete i %])
              :error-handler   #(dispatch [:event/error-alert "Unable to update sample name."])})))

(register-handler
 :event/update-sample-name-complete
 ; [validate-state]
    (let [add-id "sample-stage-detail-upload-add-file-button"]
 (fn [state [_ i response]]
   (.warn js/console "Received sample" (clj->js response))
   (assoc-in state [:search-results i] (dissoc response :project)))))

(register-handler
 :event-update-method-name-complete
 ; [validate-state]
 (fn [state [_ i response]]
   (.warn js/console "Received sample" (clj->js response))
   (assoc-in state [:search-results i] (dissoc response :project))))

; New sample name added
(register-handler
 :event/new-sample-name-changed
 [validate-state]
 (fn [state [_ project-name]]
   (.warn js/console "Received projects" (clj->js project-name))
   (.warn js/console "Received projects" (clj->js state))
   ; (dispatch [:event/>!-initchan :projects])
   (let [x (assoc state :new-sample-name project-name)]
   (.warn js/console "Received projects" (clj->js x))
   x)))

(register-handler
 :event/update-new-sample-name
 [validate-state]
 (fn [state [_ id]]
   ; (.info js/console "Received projects" (clj->js projects))
   ; (dispatch [:event/>!-initchan :projects])
  (let [name (get state :new-sample-name)]
   (.warn js/console "Received projects" (clj->js name) (clj->js state))
    (ajax-post ["projects" id "samples"]
              { :name name }
              { :handler         #(dispatch [:event/update-new-sample-name-complete %])
                :error-handler   #(dispatch [:event/error-alert "Unable to add new project."])}))))

(register-handler
 :event/update-new-sample-name-complete
 ; [validate-state]
 (fn [state [_ response]]
   (assoc state :search-results (conj (get state :search-results) (select-keys response [:id :name])))))

(register-handler
 :event/sample-search-terms-changed
 [validate-state]
 (fn [state [_ sample-name]]
   (-> null-state
       (copy-state state [[:cached]
                          [:volatile]
                          [:project]])
       (assoc :search-terms sample-name))))

(register-handler
 :event/sample-search-requested
 [validate-state]
 (fn [state _]
   (ajax-get ["projects"
              (urify (get-in state [:project :id]))
              (str "samples?q=" (urify (get state :search-terms)))]
             {:handler #(dispatch [:event/samples-retrieved %])})
   state))

(register-handler
 :event/sample-fetch-requested
 [validate-state]
 (fn [state [_ sample-id]]
   (ajax-get ["projects"
              (urify (get-in state [:project :id]))
              (str "samples")
              (urify sample-id)]
             {:handler #(dispatch [:event/samples-retrieved %])})
   state))

(register-handler
 :event/samples-retrieved
 [validate-state]
 (fn [state [_ ret]]
   (let [samples (map #(select-keys % [:id :name :project]) (if (map? ret) [ret] ret))]
     (.info js/console "%d samples returned" (count samples))
     (when (= (count samples) 1)
       (dispatch [:event/sample-selected (first samples)]))
     (assoc state :search-results samples))))

(register-handler
 :event/sample-selected
 [validate-state]
 (fn [state [_ sample]]
   (.info js/console "Retrieving sample details" (clj->js sample))
   (ajax-get ["projects"
              (urify (get-in state [:project :id]))
              "samples"
              (urify (:id sample))
              "stages"]
             {:handler #(dispatch [:event/sample-stages-retrieved %])})
   (-> null-state
       (copy-state state [[:cached]
                          [:volatile]
                          [:project]
                          [:search-terms]
                          [:search-results]])
       (assoc-in [:sample :selected] sample))))

(register-handler
 :event/sample-stages-retrieved
 [validate-state]
 (fn [state [_ rsp]]
   (.info js/console "Retrieved stages for sample"
          (clj->js (get-in state [:sample :selected]))
          (clj->js rsp))
   (.debug js/console "Expecting stages for sample: %s" (get-in state [:sample :selected :id]))
   (when-let [stages (:stages rsp)]
     (let [expected-id (get-in state [:sample :selected :id])
           actual-id   (:sample rsp)]
       (if (= expected-id actual-id)
         (-> state
             (assoc-in [:sample :stages :list] stages)
             (assoc-in [:sample :stages :token] (:token rsp)))
         (do (.warn js/console "Stages for sample %s are not for expected sample %s" actual-id expected-id)
             state))))))

;; ----------------------------------- sample stage, drilldown and input --- ;;

(register-handler
 :event/stage-selected
 [validate-state]
 (fn [state [_ stage-id]]
   (ajax-get ["projects"
              (urify (get-in state [:project :id]))
              "samples"
              (urify (get-in state [:sample :selected :id]))
              "stages"
              (urify stage-id)
              "files"]
             {:handler #(dispatch [:event/stage-details-retrieved %])})
   (assoc-in state [:sample :active-stage :id] stage-id)))

(register-handler
 :event/refresh-sample-stage-details
 [validate-state]
 (fn [state _]
   (dispatch [:event/stage-selected (get-in state [:sample :active-stage :id])])
   state))

(register-handler
 :event/stage-details-retrieved
 [validate-state]
 (fn [state [_ stage-details]]
   (.info js/console "Retrieved stage details %s" (clj->js (str stage-details)))
   (let [exp-id (get-in state [:sample :active-stage :id])
         act-id (:stage-id stage-details)]
     (if (= exp-id act-id)
       (let [fmtfn #(assoc % :status (or (#{:processing :ready} (keyword (:status %))) :unknown))
             files (map fmtfn (:files stage-details))]
         (when (some #{:processing} (map :status files))
           (do (.debug js/console "Incomplete stage file detected ... scheduling refresh")
               (.setTimeout js/window #(dispatch [:event/refresh-sample-stage-details]) (* 15 1000))))
         (assoc-in state [:sample :active-stage :file-spec] files))
       (do (.info js/console "Actual stage ID '%s' does not match expected stage ID '%s'; ignoring stage detail update"
                  act-id exp-id)
           state)))))

(register-handler
 :event/stage-method-selected
 [validate-state]
 (fn [state [_ m]]
   (assoc-in state [:sample :new-stage :method] (js->clj m :keywordize-keys true))))

(register-handler
 :event/stage-annotation-changed
 [validate-state]
 (fn [state [_ annotation]]
   (assoc-in state [:sample :new-stage :annotation] annotation)))

(register-handler
 :event/stage-added
 [validate-state]
 (fn [state [_ i m a]]
   (let [method     (s/trim (or m ""))
         annotation (s/trim (or a ""))]
     (when (and (not (empty? method)) (not (empty? annotation)))
       (let [project-id (get-in state [:project :id])
             sample-id  (get-in state [:sample :selected :id])]
         (.info js/console
                "Adding stage for sample %s,%s: method=%s, annotation=%s"
                project-id sample-id method annotation)
         (ajax-put ["projects" (urify project-id)
                    "samples"  (urify sample-id)
                    "stages"   i]
                   {:method     method
                    :annotation annotation}
                   {:handler    #(dispatch [:event/stage-persisted %])}))))
   state))

(register-handler
 :event/stage-persisted
 [validate-state]
 (fn [state [_ new-stage]]
   (dispatch [:event/sample-selected (get-in state [:sample :selected])])
   (copy-state state null-state [[:sample :new-stage]])))

;; ------------------------------------------- sample stage detail input --- ;;

(def upload-path [:sample :active-stage :upload])

(register-handler
 :task/chunking-start
 [validate-state]
 (fn [state _]
   (-> state
       (copy-state null-state [upload-path])
       (assoc-in [:volatile :digester] (Sha256.)))))

(register-handler
 :task/preprocess-chunks
 [validate-state]
 (fn [state [_ chunks]]
   (let [digester (get-in state [:volatile :digester])
         checksum (fn [chunk buf]
                    (let [len (count chunks)
                          cur (+ (.-offset chunk) 1)]
                      (when (or (= (rem cur 500) 0) (= cur len))
                        (.debug js/console "Processing chunk %d of %d" cur len))
                      (.update digester (array-buffer->utf8-array buf))
                      (dispatch [:event/upload-file-chunk-digested cur len])))
         complete (fn []
                    (let [hex (utf8-array->hex-string (.digest digester))]
                      (dispatch [:event/upload-file-checksum-computed hex])))]
     (process-file-chunks
      checksum complete (-> (first chunks) .-fileObj .-file) chunks))
   state))

(register-handler
 :event/upload-file-chunk-digested
 [validate-state]
 (fn [state [_ n cnt]]
   (assoc-in state (conj upload-path :checksum :progress) (/ n cnt))))

(register-handler
 :event/upload-file-checksum-computed
 [validate-state]
 (fn [state [_ checksum]]
   (.info js/console "Generated checksum" checksum)
   (-> state
       (assoc-in (conj upload-path :checksum :value) checksum)
       (assoc-in (conj upload-path :checksum :state) :success)
       (assoc-in (conj upload-path :checksum :progress) 1))))

(register-handler
 :event/upload-file-parts-complete
 [validate-state]
 (fn [state _]
   (.debug js/console "state @ part upload completion is:" (clj->js state))
   (let [f (get-in state (conj upload-path :file))]
     (ajax-post ["complete-multipart-upload"]
                {:upload-id       (.-uniqueIdentifier f)
                 :file-name       (.-fileName f)
                 :project         (:project state)
                 :sample          (get-in state [:sample :selected :id])
                 :sample-stage    (get-in state [:sample :active-stage :id])
                 :checksum-method :sha256
                 :checksum-value  (-> state (get-in (conj upload-path :checksum :value)))}
                {:handler         #(dispatch [:event/upload-file-complete])
                 :error-handler   #(dispatch [:event/upload-file-error "File upload error" f])}))
   (-> state
       (copy-state null-state [[:volatile :digester]]))))

(register-handler
 :event/upload-file-complete
 [validate-state]
 (fn [state _]
   (.debug js/console "file upload complete: " (get-in state (conj upload-path :file)))
   (.removeFile (get-in state [:volatile :resumable])
                (get-in state (conj upload-path :file)))
   (dispatch [:event/refresh-sample-stage-details])
   (let [new-state (assoc-in state (conj upload-path :transmit :state) :success)]
     (.debug js/console "state @ file upload completion is:" (clj->js new-state))
     (assoc-in new-state (conj upload-path :transmit :progress) 1)
     new-state)))

(register-handler
 :event/upload-file-error
 [validate-state]
 (fn [state [_ msg file]]
   (.debug js/console "File upload error!: " msg)
   (.cancel (get-in state [:volatile :resumable]))
   (let [new-state (-> state
                       (assoc-in (conj upload-path :transmit :progress) 1)
                       (assoc-in (conj upload-path :transmit :state) :error))]
     (.debug js/console "state @ file upload error is:" (clj->js new-state))
     new-state)))

(register-handler
 :event/upload-file-added
 [validate-state]
 (fn [state [_ f]]
   (.debug js/console "File added for upload.")
   (let [new-state (-> state
                       (clear    upload-path)
                       (assoc-in (conj upload-path :file) f))]
     (.debug js/console "state is now:" (clj->js new-state))
     new-state)))

(register-handler
 :event/upload-file-progress-updated
 [validate-state]
 (fn [state [_ n]]
   (if-not (= (get-in state (conj upload-path :transmit :state)) :error)
      (assoc-in state (conj upload-path :transmit :progress) n)
     state)))
